#include "noeud.h"

nd createNode(void* val){//n1
  nd res = (nd) malloc(sizeof(nd));
  res->val = val;
  res->occurence = 1;
  return res;
}//n1.

void deleteNode();

/////////////////////////////////////////

tree createTree(nd node){//t1
  tree res = (tree)malloc(sizeof(tree));
  res->racine = node;
  res->gauche = NULL;
  res->droite = NULL;
  return res;
}//t1.

void deleteTree(tree t);
void deleteNode(nd node);
void addNode(tree tFather ,tree tSon){//t4
  free(tSon->racine->code);
  tSon->racine->code = malloc((strlen(tFather->racine->code)+1)*sizeof(char));  
  if (tFather->gauche == NULL){
    tSon->racine->code = strcat(tFather->racine->code,"0");
    tFather->gauche = tSon;
  } else if (tFather->droite == NULL){
    tSon->racine->code = strcat(tFather->racine->code,"1");
    tFather->droite = tSon;
  }else printf("ERROR: tree[%c, %d] a déjà 2 enfants !",*((char*)tFather->racine->val),tFather->racine->occurence);
}//t4.
nd searchNode(void* val);

////////////////////////////////////////
tree* getOccurence(void* data, int size,
  tree*(*occurence)(void* data, int size) ){
    return (*occurence)(data, size);
}

int countOccurence(tree* trees){
  //compte le nombre de case du tableau des occurence
  int i =0;
  while (trees[i]!=NULL){
    i++;
  }
  return i;
}

static int partitionner(tree * trees, int premier, int dernier, int(superieur)(nd nd1, nd nd2)){
    tree tmp;
    tree pivot;
    pivot = trees[dernier];
    int j = premier;
    for(int i=premier; i<dernier; i++){

      if(superieur(trees[i]->racine,pivot->racine)==-1 || superieur(trees[i]->racine,pivot->racine)==0){
        // printf("-------------DEBUT\n");
        // printf("tree[i] -> %c = %i\n", *((char*)trees[i]->racine->val), pivot->racine->occurence);
        // printf("pivot   -> %c = %i\n", *((char*)pivot->racine->val), trees[i]->racine->occurence);
        // printf("-------------FIN\n");
        tmp = trees[i];
        trees[i] = trees[j];
        trees[j] = tmp;
        j = j+1;
      }
    }
    tmp = trees[dernier];
    trees[dernier] = trees[j];
    trees[j] = tmp;
    return j;
}

void triOccurence(tree * trees, int premier, int dernier, int(superieur)(nd nd1, nd nd2)){
    if(premier < dernier){
      // pivot = choix_pivot(tree * tree, premier, dernier);
      int pivot;
      pivot = partitionner(trees, premier, dernier, superieur);

      triOccurence(trees, premier, pivot-1, superieur);
      triOccurence(trees, pivot+1, dernier, superieur);
    }
}
