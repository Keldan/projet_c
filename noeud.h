#include<stdlib.h>
#include<stdio.h>
#include<string.h>

struct noeud {
    char* code;
    void* val;
    int occurence;
};
typedef struct noeud* nd;

nd createNode(void* val);


//////////////////////////////

struct arbre{
  nd racine;
  struct arbre* gauche;
  struct arbre* droite;
};

typedef struct arbre* tree;

tree createTree();
void deleteTree(tree t);
void deleteNode(nd node);
void addNode(tree tFather ,tree tSon);
nd searchNode(void* val);


/////////////

// void triOccurence(tree* trees, int deb, int fin ,int(*superieur)(nd node1, nd node2));
void triOccurence(tree* trees, int deb, int fin, int(*superieur)(nd node1, nd node2));
int countOccurence(tree* list);
tree* getOccurence(void* data, int size, tree*(*occurence)(void* data, int size));

static int superieur_char(nd node1, nd node2){
  //printf("comparaison");
  if (*((char*)node1->val) > *((char*)node2->val)) return 1;
  else if (*((char*)node1->val) < *((char*)node2->val)) return -1;
  else return 0;
}

static int superieur_occurence(nd node1, nd node2){
  //printf("comparaison");
  if (node1->occurence > node2->occurence) return 1;
  else if (node1->occurence < node2->occurence) return -1;
  else return 0;
}

static int superieur_poids(nd node1, nd node2){
  //printf("comparaison");
  int poidNd1 = node1->occurence*65 + (int)(*((char *)node1->val));
  int poidNd2 = node2->occurence*65 + (int)(*((char *)node2->val));
  if (poidNd1 > poidNd2) return 1;
  else if (poidNd1 < poidNd2) return -1;
  else return 0;
}

static tree* getOccurence_char(void* data, int size){//u1
  char* data_c = (char *) data;
  tree *liste = (tree*)malloc(size*sizeof(tree));
  liste[0] = createTree(createNode(&data_c[0]));
  int last = 0;
  int cdt;
  printf("%c\n", data_c[0]);
  for (int i=1; i < size; i++){
    cdt=0;
    for (int j=0; j < last+1; j++){
      if (data_c[i] == *((char *)liste[j]->racine->val)){
        liste[j]->racine->occurence++;
        cdt=1;
      }
    }
    if (!cdt){
      last++;
      liste[last]=(tree)malloc(sizeof(tree));
      liste[last]->racine =  createNode(&data_c[i]);
    }
  }
  return liste;
}//u1.
